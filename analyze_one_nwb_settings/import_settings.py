#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

import matplotlib.pyplot as plt
import yaml


def init_analysis(NWB_FILE=None, PC=None, PROJ=None):
    # Settings Import
    # All hard coded values are here or stored in settings files called by the
    # below settings import block.
    if NWB_FILE is None:
        NWB_FILE = 'TH405_Day34_training.nwb'
        # NWB_FILE = 'SL18_D19.nwb'
        # NWB_FILE = 'SL18_D14_S03.nwb'
        # NWB_FILE = 'FXM110_day17.nwb'

    # strip the .nwb from the file name and add to path['path']
    if PC is None:
        # PC = 'home'
        # PC = CRPC
        # PC = 'work_CR'
        PC = 'work'

    if PROJ is None:
        # PROJ = 'SL'
        # PROJ = 'beh'
        # PROJ = 'ELR'
        PROJ = 'BP'

    PATHS_FILE = 'analysis_paths.yml'
    SETTINGS_FILE = 'analyze_one_nwb_settings.yml'
    MPL_STYLE_FILE = 'jmo.mplstyle'

    code_dir = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(code_dir, PATHS_FILE), 'r') as stream:
        paths = yaml.safe_load(stream)
    with open(os.path.join(code_dir, SETTINGS_FILE), 'r') as stream:
        nwb_settings = yaml.safe_load(stream)
    paths['nwb_file'] = NWB_FILE
    paths['nwb_file_stem'] = os.path.splitext(paths['nwb_file'])[0]
    paths['pc'] = PC
    paths['proj'] = PROJ
    paths = update_paths(paths, nwb_settings)

    # Set default matplotlib styles
    plt.style.use(os.path.join(code_dir, MPL_STYLE_FILE))
    return paths, nwb_settings


def update_paths(paths, settings):
    '''
    Update paths based on PC, make sure folders exist
    '''
    # Pass PC into the script
    # File path, name variables - just add pcs as needed.
    nwb_path_var = '_'.join([paths['pc'], paths['proj'], 'nwb_path'])
    if 'nest_nwb' in settings and settings['nest_nwb']:
        paths['nwb_path'] = os.path.join(
            paths[nwb_path_var], paths['nwb_file_stem'])
    else:
        paths['nwb_path'] = paths[nwb_path_var]

    # Folder paths
    paths['figure_path'] = os.path.join(paths['nwb_path'],
                                        settings['figure_folder'])
    paths['analysis_path'] = os.path.join(paths['nwb_path'],
                                          settings['analysis_folder'])
    paths['lfp_path'] = os.path.join(paths['nwb_path'],
                                     settings['lfp_folder'])
    paths['beh_path'] = os.path.join(paths['nwb_path'],
                                     settings['beh_folder'])

    # Create the folders if they don't already exist
    if not os.path.exists(paths['figure_path']):
        os.makedirs(paths['figure_path'])
    if not os.path.exists(paths['analysis_path']):
        os.makedirs(paths['analysis_path'])
    if not os.path.exists(paths['lfp_path']):
        os.makedirs(paths['lfp_path'])
    if not os.path.exists(paths['beh_path']):
        os.makedirs(paths['beh_path'])

    return paths
