#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

import yaml

# Settings Import
# All hard coded values are here or stored in settings files
# called by the below settings import block.
BEH_CORE_SETTINGS_FILE = 'beh_core_settings.yml'
TRACKING_SETTINGS_FILE = 'tracking_core_settings_SL_Beh.yml'
# TRACK_MAP_SETTINGS_FILE = 'track_map_settings_AAM_W.yml'
# TRACK_MAP_SETTINGS_FILE = 'track_map_settings_H+4.yml'
TRACK_MAP_SETTINGS_FILE = 'track_map_settings_BP.yml'


def import_beh_core_settings(settings_file=BEH_CORE_SETTINGS_FILE):
    code_dir = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(code_dir,
                           settings_file), 'r') as stream:
        settings = yaml.safe_load(stream)
    return settings


def import_tracking_settings(settings_file=TRACKING_SETTINGS_FILE):
    code_dir = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(code_dir,
                           settings_file), 'r') as stream:
        settings = yaml.safe_load(stream)
    return settings


def import_track_map_settings(settings_file=TRACK_MAP_SETTINGS_FILE):
    code_dir = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(code_dir,
                           settings_file), 'r') as stream:
        track_map_settings = yaml.safe_load(stream)
    return track_map_settings
