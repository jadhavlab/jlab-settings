experimenter_name: Blake Porter
lab:               Shantanu Jadhav
institution:       Brandeis University
experiment_description: Inference 
session_description: Sleep - Track - Sleep
session_id:        TH405_D34_training
subject:
  description:     Long Evans Rat
  genotype:        Wild Type
  sex:             M
  species:         Ratticus norvegicus
  subject_id:      TH405
  weight:          467g

units:
  analog: unspecified
  behavioral_events: unspecified

data_acq_device:
  - system: MCU
    description: Main Control Unit from SpikeGadgets - Handles Analog and Digital Signals, and Coordination across hardware
    manufacturer: SpikeGadgets 
  - system: ECU
    description: Environmental Control Unit from SpikeGadgets - I/O control hardware
    manufacturer: SpikeGadgets 
  - system: AdaptAMaze
    description: Maze Control System
    manufacturer: JadhavLab (JMOlson)

implant_locations:
  - location: CA1_R
    nTrodes: [1,54,55,56,57,58,59,60,61,62,63,64]
  - location: CA1_L
    nTrodes: [18,19,20,21,22,23,24,25,26,27,28,29]
  - location: PFC_L,
    nTrodes: [42,43,44,45,46,47,48,49,50,51,52,53]
  - location: PFC_R
    nTrodes: [30,31,32,33,34,35,36,37,38,39,40,41]
  - location: VTA_L
    nTrodes: [2,3,4,5,6,7,8,9]
  - location: VTA_R
    nTrodes: [10,11,12,13,14,15,16,17]

cameras:
  - id: 0
    meters_per_pixel: 0.002
    manufacturer: Allied Vision 
    model: Mako G-158C
    lens: Theia SL183M
    camera_name: SleepBox
  - id: 1
    meters_per_pixel: 0.0026
    manufacturer: Allied Vision
    model: Mako G-158C
    lens: Theia SL183M
    camera_name: Room

# Epochs
# epochs are not the same as Frank lab style
# index matches task_epochs field in tasks
# id matches the unique_epoch_words field from import_settings_fullRecs
# the sleep tag used in analysis will look for an exact string match in
# task_name - default is:
# Sleep 
#
#
# LED variable logic
# led_configuration
# -- identifier to be used for analysis such as head direction and position algo 
# led_list
# -- DLC body part names
# led_positions
# -- list in order of led_list for led_configuration to analyze
# Make sure task_name is consistent through settings
epochs:
  - index: 1
    id: preSleep
    task_name: Sleep
    env_name: home_box
    led_configuration:
      - single
    led_list:
      - redled
    led_positions:
      - front
      - back

  - index: 2
    id: track
    task_name: Transitive inference training
    env_name: left_Wmaze
    led_configuration:
      - left/right
    led_list:
      - redled
      - greenled
    led_positions:
      - front
      - back
  - index: 3
    id: postSleep
    task_name: Sleep
    env_name: home_box
    led_configuration:
      - front
      - back
    led_list:
      - redled
    led_positions:
      - center
  - index: 4
    id: S04
    task_name: HomeAltVisitAll
    env_name: right_Wmaze
    led_configuration:
      - left/right
    led_list:
      - redled
      - greenled
    led_positions:
      - right
      - left
  - index: 5
    id: S05
    task_name: Sleep
    env_name: home_box
    led_configuration:
      - single
    led_list:
      - redled
    led_positions:
      - center
  - index: 6
    id: S06
    task_name: HomeAltVisitAll
    env_name: right_Wmaze
    led_configuration:
      - left/right
    led_list:
      - redled
      - greenled
    led_positions:
      - right
      - left
  - index: 7
    id: S07
    task_name: Sleep
    env_name: home_box
    led_configuration:
      - single
    led_list:
      - redled
    led_positions:
      - center

environments:
  - env_name: home_box
    event_list:
  - env_name: left_Wmaze
    event_list:
      - Reward Well 1
      - Reward Well 2
      - Reward Well 3
      - Reward Well 4
      - Reward Pump 1
      - Reward Pump 2
      - Reward Pump 3
      - Reward Pump 4
  - env_name: right_Wmaze
    event_list:
      - Reward Well 5
      - Reward Well 6
      - Reward Well 7
      - Reward Well 8
      - Reward Pump 1
      - Reward Pump 2
      - Reward Pump 3
      - Reward Pump 4


tasks:
  - task_name:          Sleep
    task_description:   The animal sleeps in a small empty box.
    camera_id:
      - 0
    task_epochs:
      - 1
      - 3
      - 5
      - 7
  - task_name:            HomeAltVisitAll
    task_description:     Shuttle task between home and 4 destinations. 
    camera_id:
      - 1
    task_epochs:
      - 2
      - 4
      - 6


behavioral_events:
  - id: ECU_Din1
    description: Reward Well 1
  - id: ECU_Din2
    description: Reward Well 2
  - id: ECU_Din3
    description: Reward Well 3
  - id: ECU_Din4
    description: Reward Well 4
  - id: ECU_Din5
    description: Reward Well 5
  - id: ECU_Din6
    description: Reward Well 6
  - id: ECU_Din7
    description: Reward Well 7
  - id: ECU_Din8
    description: Reward Well 8
  - id: ECU_Dout1
    description: Reward Pump 1
  - id: ECU_Dout2
    description: Reward Pump 2
  - id: ECU_Dout3
    description: Reward Pump 3
  - id: ECU_Dout4
    description: Reward Pump 4
  - id: ECU_Dout5
    description: Reward Pump 5
  - id: ECU_Dout6
    description: Reward Pump 6
  - id: ECU_Dout7
    description: Reward Pump 7
  - id: ECU_Dout8
    description: Reward Pump 8
  - id: ECU_Dout17
    description: Barrier 1
  - id: ECU_Dout18
    description: Barrier 2
  - id: ECU_Dout19
    description: Barrier 3
  - id: ECU_Dout20
    description: Barrier 4
  - id: ECU_Dout21
    description: Barrier 5
  - id: ECU_Dout22
    description: Barrier 6
  - id: ECU_Dout23
    description: Barrier 7
  - id: ECU_Dout24
    description: Barrier 8
      
