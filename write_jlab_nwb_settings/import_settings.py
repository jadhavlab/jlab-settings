#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

import yaml

PATHS_FILE = 'jlab_nwb_paths.yml'
# IMPORT_SETTINGS_FILE = 'write_jlab_nwb_settings.yml'
IMPORT_SETTINGS_FILE = 'write_jlab_nwb_settings_fullRecs.yml'
# IMPORT_SETTINGS_FILE = 'write_jlab_nwb_settings_behOnly.yml'

code_dir = os.path.dirname(os.path.realpath(__file__))


def get_paths(paths_yml_file=None):
    if paths_yml_file is None:
        paths_yml_file = os.path.join(code_dir, PATHS_FILE)
    with open(paths_yml_file, 'r') as stream:
        try:
            paths = yaml.safe_load(stream)
            return paths
        except yaml.YAMLError as exc:
            print(exc)
            return


def get_settings(settings_yml_file=None):
    if settings_yml_file is None:
        settings_yml_file = os.path.join(code_dir, IMPORT_SETTINGS_FILE)
    with open(settings_yml_file, 'r') as stream:
        try:
            settings = yaml.safe_load(stream)
            return settings
        except yaml.YAMLError as exc:
            print(exc)
            return
